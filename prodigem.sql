CREATE TABLE `prodigem_torrents` (
  nid INT NOT NULL,
  page VARCHAR(255),
  url VARCHAR(255),
  PRIMARY KEY (nid)
);