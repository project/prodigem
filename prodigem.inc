<?php
// most of this is taken from PEP: http://prodigem.com/code/pep/pep.txt
// This object parses XML and turns it into an array
// Code for this object based from http://us2.php.net/xml_parse
class prodigemXML_Array_Producer 
{
	var $arr_output = array();
	var $res_parser;
	var $str_xml_data;
  
	function Parse($str_input_xml) {
		$this->res_parser = xml_parser_create();
	  xml_set_object($this->res_parser, $this);
    xml_set_element_handler($this->res_parser, "TagOpen", "TagClosed");
	  xml_set_character_data_handler($this->res_parser, "TagData");
		$this->str_xml_data = xml_parse($this->res_parser, $str_input_xml);
		
    if (!$this->str_xml_data) {
      die(sprintf("XML error: %s at line %d",
      xml_error_string(xml_get_error_code($this->res_parser)),
      xml_get_current_line_number($this->res_parser)));
    }
                          
    xml_parser_free($this->res_parser);
		return $this->arr_output;
	}

	function TagOpen($parser, $name, $attrs) {
    $tag = array("name"=>$name, "attrs"=>$attrs);
    array_push($this->arr_output, $tag);
  }
  
  function TagData($parser, $tag_data) {
		if (trim($tag_data)) {
			if (isset($this->arr_output[count($this->arr_output)-1]['tag_data'])) {
        $this->arr_output[count($this->arr_output)-1]['tag_data'] .= $tag_data;
      } 
      else {
        $this->arr_output[count($this->arr_output)-1]['tag_data'] = $tag_data;
      }
    }
  }
  
  function TagClosed($parser, $name) {
    $this->arr_output[count($this->arr_output)-2]['children'][] = $this->arr_output[count($this->arr_output)-1];
 		array_pop($this->arr_output);
 	}
}	

// This function takes a response from Prodigem's REST API and returns only the XML from it
function _prodigem_get_rest_xml($buf) {
	ereg("<\?xml.*<\/rsp>", $buf, $regs);
  return $regs[0];
}

// This function calls into the Prodigem API and returns an XML_Array of the result
function prodigem_invoke_rest_api($rest_method, $data_array) {
	$host = 'ssl4.westserver.net';
	$method = 'POST';
	$path = '/lerhaupt/prodigem/services/rest.php';
	$data = "method=$rest_method";   
  $transport = function_exists('openssl_open') ? 'https://' : 'http://';
  $port = function_exists('openssl_open') ? 443 : 80;
	$prodigem_url = $transport . $host .':'. $port . $path;
	$headers = array(
	  'Content-Type' => 'application/x-www-form-urlencoded'
  );
	
	foreach ($data_array as $key => $element) {
		$data .= "&$key=$element";
	}

  $response = drupal_http_request($prodigem_url, $headers, $method, $data);
  
  if (!isset($response->error)) {
	  $xml_rest = _prodigem_get_rest_xml($response->data);

	  $xml_object = new prodigemXML_Array_Producer();
	  return $xml_object->Parse($xml_rest);
	}
	else {
	  watchdog('http', $response->code .': '. $response->error);
  }
}

// This function inspects an XML_Array and prints any errors 
function prodigem_print_api_error($xml_array) {
  if ($xml_array[0]['attrs']['STAT'] == 'ok') {
	  return 0;
	}
	else {
	  $output = "The Prodigem API call to method \"".$xml_array[0]['children'][0]['tag_data']."\" resulted in the following errors:\n";
		$count = 1;
	  foreach ($xml_array[0]['children'][1]['children'] as $error_array) {
		  $output .= "$count. ".$error_array['attrs']['MSG']."\n";
			$count++;
		}
		watchdog('error', $output, WATCHDOG_ERROR);
		return 1;
	}
}

// This function takes the top level of an XML_Array and returns an associative array
function make_associative_array($xml_array_portion) {
  $return_array = array();
  
  foreach ($xml_array_portion AS $sub_array) {
	 	if ($sub_array['tag_data']) {
			$return_array[$sub_array['name']] = $sub_array['tag_data'];
		}
		else if ($sub_array['attrs']) {
			$return_array[$sub_array['name']] = $sub_array['attrs'];
		}
  }
  
	return $return_array;
}
// api calls

// authentication

/* Starts a Prodigem session
 * @sa http://torrentocracy.com/mediawiki/index.php/Prodigem.auth.startSession
 * @ingroup prodigem
 */
function prodigem_api_start_session($username, $passwd) {
  $data_array = array();
	$data_array['username'] = $username;
	$data_array['password'] = $passwd;
	$return_array = array();
	$xml_array = prodigem_invoke_rest_api('prodigem.auth.startSession', $data_array); 

	if (!prodigem_print_api_error($xml_array)) {
	  $xml_assoc = make_associative_array($xml_array[0]['children']);
	  $return_array['userid'] = $xml_assoc['USERID'];
	  $return_array['session'] = $xml_assoc['SESSION'];
	  return $return_array;
	}
}

/* End a Prodigem session
 * @sa http://torrentocracy.com/mediawiki/index.php/Prodigem.auth.endSession
 * @ingroup prodigem
 */
function prodigem_api_end_session($user_id, $session) {
  $data_array = array();
	$data_array['userid'] = $user_id;
	$data_array['session'] = $session;
	$xml_array = prodigem_invoke_rest_api('prodigem.auth.endSession', $data_array); 

	if (!prodigem_print_api_error($xml_array)) {
	  $xml_assoc = make_associative_array($xml_array[0]['children']);
	  return 1;
	}
}

// file upload
// upload_info is assoc. array
function _prodigem_api_upload_info($xml_assoc) {
	$upload_info = array();
	
  $upload_info['fileid'] = $xml_assoc['FILEID'];
  $upload_info['filename'] = $xml_assoc['FILENAME'];
  $upload_info['folderid'] = $xml_assoc['FOLDERID'];
  $upload_info['foldername'] = $xml_assoc['FOLDERNAME'];	  
  
  return $upload_info;
}

/* Upload a file to Prodigem
 * @sa http://torrentocracy.com/mediawiki/index.php/Prodigem.upload.postFile
 * @ingroup prodigem
 */
function prodigem_api_post_file($user_id, $session, $file_contents) {
  $data_array = array();
	$data_array['userid'] = $user_id;
	$data_array['session'] = $session;
	$data_array['file'] = $file_contents;
	$xml_array = prodigem_invoke_rest_api('prodigem.upload.postFile', $data_array); 

	if (!prodigem_print_api_error($xml_array)) {
	  $xml_assoc = make_associative_array($xml_array[0]['children']);
	  return _prodigem_api_upload_info($xml_assoc);
	}
}

/* This method can be used to pull content into your Prodigem account from a URL.
 * @sa http://torrentocracy.com/mediawiki/index.php/Prodigem.upload.grabURL
 * @ingroup prodigem
 */
function prodigem_api_grab_url($user_id, $session, $url) {
  $data_array = array();
	$data_array['userid'] = $user_id;
	$data_array['session'] = $session;
	$data_array['url'] = $url;
	$xml_array = prodigem_invoke_rest_api('prodigem.upload.grabURL', $data_array); 

	if (!prodigem_print_api_error($xml_array)) {
	  $xml_assoc = make_associative_array($xml_array[0]['children']);
	  return _prodigem_api_upload_info($xml_assoc);
	}
}

// file management

/* Move a file in your Prodigem account to a different folder.
 * @sa http://torrentocracy.com/mediawiki/index.php/Prodigem.files.moveFile
 * @ingroup prodigem
 */
function prodigem_api_move_file($user_id, $session, $file_id, $folder_id) {
  $data_array = array();
	$data_array['userid'] = $user_id;
	$data_array['session'] = $session;
	$data_array['fileid'] = $file_id;
	$data_array['folderid'] = $folder_id;
	$xml_array = prodigem_invoke_rest_api('prodigem.files.moveFile', $data_array); 

	return !prodigem_print_api_error($xml_array);
}


/* Delete a file in your Prodigem account.
 * @sa http://torrentocracy.com/mediawiki/index.php/Prodigem.files.deleteFile
 * @ingroup prodigem
 */
function prodigem_api_delete_file($user_id, $session, $file_id) {
  $data_array = array();
	$data_array['userid'] = $user_id;
	$data_array['session'] = $session;
	$data_array['fileid'] = $file_id;
	$xml_array = prodigem_invoke_rest_api('prodigem.files.deleteFile', $data_array); 

	return !prodigem_print_api_error($xml_array);
}

// This function gets the users file list as an XML_Array

/* Get a list of files in your Prodigem account.
 * @sa http://torrentocracy.com/mediawiki/index.php/Prodigem.files.getListUser
 * @ingroup prodigem
 */
function prodigem_get_user_files($user_id, $session, $folder_id = 0) {
  $return_array = array();
  $response_array = array();
  $data_array = array();
	$data_array['userid'] = $user_id;
	$data_array['session'] = $session;
	if ($folder_id) {
	  $data_array['folderid'] = $folder_id;
  }
	$response_array = prodigem_invoke_rest_api('prodigem.files.getListUser', $data_array); 

	if (!prodigem_print_api_error($response_array)) {
  	foreach ($response_array[0]['children'][2]['children'] as $file) {
  	  $current_file = array();
  	  
  	  foreach ($file['attrs'] as $key => $val) {
  	    $current_file[strtolower($key)] = $val;
      }
      
      $return_array[] = $current_file;
	  }
	  
		return $return_array;
	}
}

// folder management

/* Create a folder in your Prodigem account.
 * @sa http://torrentocracy.com/mediawiki/index.php/Prodigem.folders.createFolder
 * @ingroup prodigem
 */
function prodigem_api_create_folder($user_id, $session, $folder_name) {
  $data_array = array();
  $return_array = array();
	$data_array['userid'] = $user_id;
	$data_array['session'] = $session;
	$data_array['foldername'] = $folder_name;
	$xml_array = prodigem_invoke_rest_api('prodigem.folders.createFolder', $data_array); 
	
	if (!prodigem_print_api_error($xml_array)) {
	  $xml_assoc = make_associative_array($xml_array[0]['children']);
	  $return_array['folderid'] = $xml_assoc['FOLDERID'];
	  $return_array['foldername'] = $xml_assoc['FOLDERNAME'];
	  return $return_array;
	}
}

/* Delete a folder in your Prodigem account.
 * @sa http://torrentocracy.com/mediawiki/index.php/Prodigem.folders.deleteFolder
 * @ingroup prodigem
 */
function prodigem_api_delete_folder($user_id, $session, $folder_id) {
  $data_array = array();
	$data_array['userid'] = $user_id;
	$data_array['session'] = $session;
	$data_array['folderid'] = $folder_id;
	$xml_array = prodigem_invoke_rest_api('prodigem.folders.deleteFolder', $data_array); 

	return !prodigem_print_api_error($xml_array);
}

/* Get a list of folders in your Prodigem account.
 * @sa http://torrentocracy.com/mediawiki/index.php/Prodigem.folders.getListUser
 * @ingroup prodigem
 */
function prodigem_api_get_user_folders($user_id, $session) {
  $return_array = array();
  $response_array = array();
  $data_array = array();
	$data_array['userid'] = $user_id;
	$data_array['session'] = $session;
	$response_array = prodigem_invoke_rest_api('prodigem.folders.getListUser', $data_array); 

	if (!prodigem_print_api_error($response_array)) {
  	foreach ($response_array[0]['children'][2]['children'] as $folder) {
  	  $current_folder = array();
  	  
  	  foreach ($folder['attrs'] as $key => $val) {
  	    $current_folder[strtolower($key)] = $val;
      }
      
      $return_array[] = $current_folder;
	  }
	  
		return $return_array;
	}
}

// torrent management

/* Create a torrent from the files in a folder.
 * @sa http://torrentocracy.com/mediawiki/index.php/Prodigem.torrents.createTorrent
 * @ingroup prodigem
 */
function prodigem_api_create_torrent($user_id, $session, $folder_id, $category_id, $torrent_hosted, $torrent_tags, $torrent_title, $torrent_description, $torrent_price, $license_type, $license_options, $series_id = 0) {
 	$return_array = array();
  $data_array = array();
  $torrent = array();
	$data_array['userid'] = $user_id;
	$data_array['session'] = $session;
	$data_array['folderid'] = $folder_id;
	$data_array['categoryid'] = $category_id;
	$data_array['torrenthosted'] = $torrent_hosted;
	if ($torrent_tags != '') {
	  $data_array['torrenttags'] = $torrent_tags;
	}
	$data_array['torrenttitle'] = $torrent_title;
	$data_array['torrentdescription'] = $torrent_description;
	$data_array['torrentprice'] = ($torrent_price != '') ? $torrent_price : '0.00';
	$data_array['licensetype'] = $license_type;
	foreach ($license_options as $key => $val) {
	  $data_array[$key] = $val;
  }
  if ($series_id > 0) {
    $data_array['seriesid'] = $series_id;
  }
  	
	$return_array = prodigem_invoke_rest_api('prodigem.torrents.createTorrent', $data_array);
	
	if (!prodigem_print_api_error($return_array)) {	
	  $return_assoc = make_associative_array($return_array[0]['children']);
	  $torrent['page'] = $return_assoc['TORRENTPAGE'];
	  $torrent['url'] = $return_assoc['TORRENTURL'];

	  return $torrent;
	}
}

/* Edit a torrent in your Prodigem account.
 * @sa http://torrentocracy.com/mediawiki/index.php/Prodigem.torrents.editTorrent
 * @ingroup prodigem
 */
function prodigem_api_edit_torrent($user_id, $session, $torrent_id, $category_id, $torrent_hosted, $torrent_tags, $torrent_title, $torrent_description, $series_id = 0) {
 	$return_array = array();
  $data_array = array();
	$data_array['userid'] = $user_id;
	$data_array['session'] = $session;
	$data_array['torrentid'] = $torrent_id;
	$data_array['categoryid'] = $category_id;
	$data_array['torrenthosted'] = $torrent_hosted;
	if ($torrent_tags != '') {
	  $data_array['torrenttags'] = $torrent_tags;
	}
	$data_array['torrenttitle'] = $torrent_title;
	$data_array['torrentdescription'] = $torrent_description;
  if ($series_id > 0) {
    $data_array['seriesid'] = $series_id;
  }
	
	$return_array = prodigem_invoke_rest_api('prodigem.torrents.editTorrent', $data_array);
	
	return !prodigem_print_api_error($return_array);
}

/* Delete a torrent in your Prodigem account.
 * @sa http://torrentocracy.com/mediawiki/index.php/Prodigem.torrents.deleteTorrent
 * @ingroup prodigem
 */
function prodigem_api_delete_torrent($user_id, $session, $torrent_id) {
 	$return_array = array();
  $data_array = array();
	$data_array['userid'] = $user_id;
	$data_array['session'] = $session;
	$data_array['torrentid'] = $torrent_id;

	$return_array = prodigem_invoke_rest_api('prodigem.torrents.deleteTorrent', $data_array);
	
	return !prodigem_print_api_error($return_array);
}

/* Orphan a torrent in your Prodigem account.
 * @sa http://torrentocracy.com/mediawiki/index.php/Prodigem.torrents.orphanTorrent
 * @ingroup prodigem
 */
function prodigem_api_orphan_torrent($user_id, $session, $torrent_id) {
 	$return_array = array();
  $data_array = array();
	$data_array['userid'] = $user_id;
	$data_array['session'] = $session;
	$data_array['torrentid'] = $torrent_id;

	$return_array = prodigem_invoke_rest_api('prodigem.torrents.orphanTorrent', $data_array);
	
	return !prodigem_print_api_error($return_array);
}

function _prodigem_api_parse_torrents($response_array) {
  $return_array = array();
  
 	foreach ($response_array[0]['children'][2]['children'] as $torrent) {
    $current_torrent = array();
  	  
    foreach ($torrent['attrs'] as $key => $val) {
      $current_torrent[strtolower($key)] = $val;
    }
      
    $return_array[] = $current_torrent;
  }
  
  return $return_array;
}

/* Get a listing of torrents in your account.
 * @sa http://torrentocracy.com/mediawiki/index.php/Prodigem.torrents.getListUser
 * @ingroup prodigem
 */
function prodigem_api_get_user_torrents($user_id, $session, $category_id = 0) {
 	$return_array = array();
  $data_array = array();
  $torrents = array();
	$data_array['userid'] = $user_id;
	$data_array['session'] = $session;
	if ($category_id > 0) {
	  $data_array['categoryid'] = $category_id;
  }
  
	$return_array = prodigem_invoke_rest_api('prodigem.torrents.getListUser', $data_array);
	
	if (!prodigem_print_api_error($return_array)) {
	  $torrents = _prodigem_api_parse_torrents($return_array);
	  
	  return $torrents;
	}
}

/* Get a public listing of torrents available on Prodigem.com.
 * @sa http://torrentocracy.com/mediawiki/index.php/Prodigem.torrents.getListPublic
 * @ingroup prodigem
 */
function prodigem_api_get_public_torrents($user_id, $category_id = 0) {
 	$return_array = array();
  $data_array = array();
  $torrents = array();
	$data_array['userid'] = $user_id;
	if ($category_id > 0) {
	  $data_array['categoryid'] = $category_id;
  }
  
	$return_array = prodigem_invoke_rest_api('prodigem.torrents.getListPublic', $data_array);
	
	if (!prodigem_print_api_error($return_array)) {
	  $torrents = _prodigem_api_parse_torrents($return_array);
	  
	  return $torrents;
	}
}

// licenses

/* Get a listing of licenses to place your work under.
 * @sa http://torrentocracy.com/mediawiki/index.php/Prodigem.licenses.getListPublic
 * @ingroup prodigem
 */
function prodigem_api_get_licenses($license_type = '', $license_id = 0, $license_attribution = '', $license_commercialuse = '', $license_modifications = '', $license_sample = '') {
  $return_array = array();
  $response_array = array();
  $data_array = array();
  if ($license_type != '') {
	  $data_array['licensetype'] = $license_type;
	}
	if ($license_id > 0) {
	  $data_array['licenseid'] = $license_id;
  }
  if ($license_attribution != '') {
	  $data_array['licenseattribution'] = $license_attribution;
	}
  if ($license_commercialuse != '') {
	  $data_array['licensecommercialuse'] = $license_commercialuse;
	}
  if ($license_modifications != '') {
	  $data_array['licensemodifications'] = $license_modifications;
	}
  if ($license_sample != '') {
	  $data_array['licensesample'] = $license_sample;
	}
	$response_array = prodigem_invoke_rest_api('prodigem.licenses.getListPublic', $data_array); 

	if (!prodigem_print_api_error($response_array)) {
  	foreach ($response_array[0]['children'][2]['children'] as $license_group) {
  	  $current_group = $license_group['attrs']['TYPE'];
  	  foreach ($license_group['children'] as $license) {
  	    $current_license = array();
  	    foreach ($license['attrs'] as $key => $value) {
  	      $current_license[strtolower($key)] = $value;
        }
        $current_license['licensetype'] = $current_group;
        $return_array[$current_license['licenseid']] = $current_license;
      }
	  }
	  
		return $return_array;
	}
}

function prodigem_api_license_get_options($license) {
  switch ($license['licensetype']) {
    case 'licenses_cc':
      return array(
        'licenseattribution' => $license['licenseattribution'],
        'licensecommercialuse' => $license['licensecommercialuse'],
        'licensemodifications' => $license['licensemodifications']
      );
      break;
    case 'licenses_cc_sampling':
      return array(
        'licensesample' => $license['licensesample'],
      );
      break;
    default:
      $lname = $license['licensename'];
      if (isset($license['licensesubname'])) {
        $lname .= '/'. $license['licensesubname'];
      }
      return array(
        'licenseothername' => $lname,
        'licenseotherurl' => $license['licenseurl']
      );
      break;
  }
}

function prodigem_api_licenses2assoc($licenses) {
  $ret = array();
  
  foreach ($licenses as $license) {
    $ret[$license['licenseid']] = $license['licensename'] .'/'. $license['licensesubname'];
  }
  
  natsort($ret);
  return $ret;
}

function prodigem_get_licenses() {
  if ($cached = cache_get('prodigem_licenses')) {
    return unserialize($cached);
  }
  else {
    $licenses = prodigem_api_get_licenses();
    cache_set('prodigem_licenses', serialize($licenses));
    return $licenses;
  }
}

// categories

/* Get a listing of categories for torrents.
 * @sa http://torrentocracy.com/mediawiki/index.php/Prodigem.categories.getListPublic
 * @ingroup prodigem
 */
function prodigem_api_get_categories($category_id = 0) {
  $return_array = array();
  $response_array = array();
  $data_array = array();
  if ($category_id > 0) {
	  $data_array['categoryid'] = $category_id;
	}
	$response_array = prodigem_invoke_rest_api('prodigem.categories.getListPublic', $data_array); 

	if (!prodigem_print_api_error($response_array)) {
  	foreach ($response_array[0]['children'][2]['children'] as $category) {
  	  $category_attrs = $category['attrs'];
  	  $return_array[$category_attrs['CATEGORYID']] = $category_attrs['CATEGORYNAME'];
	  }
	  
		return $return_array;
	}
}

function prodigem_get_categories($category_id = 0) {
  if ($category_id) {
    return prodigem_api_get_categories($category_id);
  }
  else {
    if ($cached = cache_get('prodigem_categories')) {
      return unserialize($cached);
    }
    else {
      $categories = prodigem_api_get_categories();
      cache_set('prodigem_categories', serialize($categories));
      return $categories;
    }
  }
}

// series

/* Get a listing of series in your Prodigem account.
 * @sa http://torrentocracy.com/mediawiki/index.php/Prodigem.series.getListUser
 * @ingroup prodigem
 */
function prodigem_api_get_user_series($user_id, $session, $series_id = 0) {
  $return_array = array();
  $response_array = array();
  $data_array = array();
	$data_array['userid'] = $user_id;
	$data_array['session'] = $session;
	if ($series_id > 0) {
	  $data_array['seriesid'] = $series_id;
  }
	$response_array = prodigem_invoke_rest_api('prodigem.series.getListUser', $data_array); 

	if (!prodigem_print_api_error($response_array)) {
  	foreach ($response_array[0]['children'][2]['children'] as $series) {
  	  $current_series = array();
  	  
  	  foreach ($series['attrs'] as $key => $val) {
  	    $current_series[strtolower($key)] = $val;
      }
      
      $return_array[] = $current_series;
	  }
	  
		return $return_array;
	}
}