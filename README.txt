name:    prodigem module
license: GPL
by:      Breyten Ernsting (bje@dds.nl)

requirements: Drupal 4.6.3 or above, upload module needs to be enabled.

1. Copy the files to a directory of your choice (say, site/modules/prodigem)
2. execute the statements in the prodigem.sql file in phpmyadmin, or via the command line.
3. Activate the module in admin/modules
4. Go the settings page for this module: admin/settings/prodigem
5. fill in your Prodigem user & password and adjust the other options, and save the settings.
6. enjoy!